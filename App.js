import { StyleSheet, Text, View,TouchableWithoutFeedback,Keyboard  } from 'react-native';
import Header from './components/Header';
import { StatusBar } from 'expo-status-bar'
import StartingPage from './screens/StartingPage';
import GameScreen from './screens/GameScreen';


export default function App() {
  return (
    <TouchableWithoutFeedback onPress={()=> Keyboard.dismiss()}>
    <View style={styles.container}>
      <Header header="Guess a number"/>
      <StartingPage/>
      <StatusBar hidden />
    </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
