import { View, Text,Button} from 'react-native'
import React,{useState} from 'react'

const generateRandomBetween=(min,max,exclude)=>{
    min=Math.ceil(min)
    max=Math.floor(max)
    const rndNum = Math. floor(Math. random()* (max-min)) + min;
    if (rndNum == exclude) {
    return generateRandomBetween (min, max, exclude) }
    else {
    return rndNum;
    }
}
const GameScreen = () => {
    const[currentGuess,setCurrentGuess]=useState(generateRandomBetween(1,100,props.userChoice))
  return (
    <View>
      <Text>Opponents guess</Text>
      <Text>{currentGuess}</Text>
      <Button title='Upper'/>
      <Button title='lower'/>
    </View>
  )
}

export default GameScreen