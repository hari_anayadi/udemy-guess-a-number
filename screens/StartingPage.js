import { View, Text, StyleSheet, Button ,Alert,Keyboard} from "react-native";
import React, { useState } from "react";
import Colors from "../components/Colors";
import TextInputComponent from "../components/TextInputComponent";
import NUmberContainer from "../components/NUmberContainer";

const StartingPage = () => {
  const [enteredValue, setEnteredVlaue] = useState("");
  const [confirmed, setConfirmed] = useState(false);
  const [selectedNumber, setSelectedNumber] = useState();

  const InputNumberHandler = (inputText) => {
    setEnteredVlaue(inputText.replace(/[^0-9]/g, ""));
  };

  const ResetInputHandler = () => {
    setEnteredVlaue("");
    setConfirmed(false);
  };

  const ConfirmInputHandler = () => {
    const chosenNumber = parseInt(enteredValue);
    if (chosenNumber == NaN || chosenNumber <= 0 || chosenNumber > 99) {
      Alert.alert('invalid number',
      'number has to be 1 and 99',[{text:"okay",style:"destructive",onPress:ResetInputHandler}])
    }
    setConfirmed(true);
    setSelectedNumber(chosenNumber);
    setEnteredVlaue("");
    Keyboard.dismiss()
  };
  let confirmedOutput;
  if(confirmed){
    confirmedOutput=<NUmberContainer number={selectedNumber}>
    </NUmberContainer>
  }
  return (
    <View style={styles.Container}>
      <Text style={styles.Title}>Entry Page</Text>
      <View style={styles.TextInputContainer}>
        <Text>select a number</Text>
        <TextInputComponent
          onChangeText={InputNumberHandler}
          style={styles.TextInput}
          keyboardType="number-pad"
          maxLength={2}
          value={enteredValue}
        />

        <View style={styles.ButtonContainer}>
          <Button
            title="Reset"
            onPress={() => ResetInputHandler()}
            color={Colors.primary}
          />
          <Button title="Next" onPress={ConfirmInputHandler} color={Colors.accent} />
        </View>
      </View>
      {confirmedOutput}
    </View>
  );
};
const styles = StyleSheet.create({
  Container: {
    flex: 1,
    padding: 10,
    alignItems: "center",
  },
  Title: {
    fontSize: 20,
    marginVertical: 10,
  },
  TextInputContainer: {
    width: "80%",
    alignItems: "center",
    elevation: 5,
    backgroundColor: "white",
    padding: 20,
    borderRadius: 10,
  },
  ButtonContainer: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    paddingHorizontal: 20,
  },
  TextInput: {
    width: 50,
    textAlign: "center",
  },
});

export default StartingPage;
