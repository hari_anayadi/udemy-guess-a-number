import { View, Text,StyleSheet } from 'react-native'
import React from 'react'
import Colors from './Colors'
import { color } from 'react-native/Libraries/Components/View/ReactNativeStyleAttributes'

const Header = (props) => {
  return (
    <View style={styles.Container}>
      <Text style={styles.HeaderTitle}>{props.header}</Text>
    </View>
  )
}

const styles=StyleSheet.create({
    Container:{
        width:"100%",
        height:100,
        padding:35,
        backgroundColor:Colors.header,
        alignItems:"center",
        justifyContent:"center"
    },
    HeaderTitle:{
        color:"black",
        fontSize:20,
        fontWeight:"bold"
    }
})

export default Header