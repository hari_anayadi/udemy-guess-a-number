import { View, Text, StyleSheet, Button } from "react-native";
import React from "react";
import Colors from "./Colors";

const NUmberContainer = (props) => {
  return (
    <View style={styles.container}>
      <Text>You selected</Text>
      <View style={styles.SelectedNumberContainer}>
        <Text style={styles.SelectedNumberText}>{props.number}</Text>
      </View>
      <Button title="Start game" color={Colors.accent} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    width: "80%",
    alignItems: "center",
    elevation: 5,
    backgroundColor: "white",
    padding: 20,
    borderRadius: 10,
    marginTop: 20,
    justifyContent: "space-between",
  },
  SelectedNumberContainer: {
    padding: 15,
    borderWidth: 2,
    marginVertical: 10,
    borderTopColor: Colors.primary,
    borderBottomColor: Colors.accent,
    borderRightColor: Colors.header,
    borderLeftColor: Colors.header,
    borderRadius:20
  },
  SelectedNumberText: {
    fontSize: 20,
  },
});
export default NUmberContainer;
