import { TextInput,StyleSheet} from 'react-native'
import React from 'react'

const TextInputComponent = props => {
  return (
      <TextInput {...props} style={{...styles.TextInput,...props.style}}/>
  )
}

const styles=StyleSheet.create({
    TextInput:{ 
        height:40,
        borderBottomColor:"gray",
        marginVertical:10,
        borderBottomWidth:1,
    }
})

export default TextInputComponent